const mysql = require('mysql2')

const pool = mysql.createPool({
    host: "db",
    user: 'root',
    password: 'kari1234',
    waitForConnections: true,
    connectionLimit: 15,
    database: 'movie_db',
})

module.exports = {
    pool,
}